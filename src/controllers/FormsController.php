<?php

namespace App\packages\forms\src\controllers;

use App\common\Common;
use App\Http\Controllers\Controller;
use App\ErrorlogModel;
use App\GeneralMaster;
use App\LoggingReport;
use App\packages\forms\src\models\MasterModel;
use Carbon\Carbon;
use Dingo\Api\Auth\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class FormsController extends Controller
{

    public function index(Request $request)
    {
        $requestData = $request->all();

        $sessionData = $request->session()->get('user_info');

        try {
            $key            = $request->key;
            $data_id        = $request->id;
            $multiSaveKey   = $request->multiSaveKey;

            $form_data      = DB::table('forms')->where('form_key', $key)->first();
            $field_data     = DB::table('form_fields')->where('form_id', $form_data->id)->where('is_deleted', 0)
                            ->orderBy('sequence', 'asc')->get();

            $table_name     = $form_data->table_name;
            $columns        = 12 / ($form_data->display_columns==0?3:$form_data->display_columns);

            if(empty($multiSaveKey)) {
                if ($data_id != 0) {
                    //get data from relevant table
                    $editEntry = DB::table($table_name)->where('id', $data_id)->first();
                    $editEntry = json_decode(json_encode($editEntry), true);
                } else {
                    $editEntry = null;
                }
            }else{
                $url = "App\Http\Controllers\\".$multiSaveKey;
                $data = app($url)->$key($data_id);
                $editEntry = $editEntry = json_decode(json_encode($data['editEntry']), true);
            }

        } catch (\Illuminate\Database\QueryException $ex) {
            // $common     = new Common();
            // $common->error_logging($ex, 'index', 'FormController.php');
            // return view('errors.oh!');
            dd($ex->getMessage());
            dd($ex->getLine());
        }
        return view('forms::forms.generic_add_edit', compact('form_data', 'field_data', 'columns', 'editEntry','sessionData'));
    }

    public function genericSave(Request $request)
    {
        try {
          //  dd($request);
            $sessionData = $request->session()->get('user_info');
            $currentDate = Carbon::now();

            if ($request->type == 'insert') {
                $s_key = $request->key;
               // dd($s_key);
                $s_table_data = $request->except(['type', 'key']);

                $s_form_data  = DB::table('forms')->where('form_key', $s_key)->first();
                $s_table_name = $s_form_data->table_name;

                $s_field_data = DB::table('form_fields')->where('form_id', $s_form_data->id)->get();

                $s_data_to_insert = array();

               // $s_data_to_insert['created_by'] = $sessionData['user_id'];
               // $s_data_to_insert['updated_by'] = $sessionData['user_id'];

                $s_data_to_insert['created_by'] = !empty($sessionData['user_id']) ? $sessionData['user_id']:0;
                $s_data_to_insert['updated_by'] = !empty($sessionData['user_id']) ? $sessionData['user_id']:0;
                $s_data_to_insert['created_at'] = $currentDate->toDateTimeString();
                $s_data_to_insert['updated_at'] = $currentDate->toDateTimeString();
                $s_data_to_insert['is_deleted'] = 0;
                foreach ($s_field_data as $field) {
                    $s_data_to_insert[$field->db_mapping] = !empty($s_table_data[$field->name]) ? $s_table_data[$field->name] :0;
                }

                DB::table($s_table_name)->insert($s_data_to_insert);
            } else if ($request->type == 'update') {
                $u_entry_id = $request->id;
                $u_key = $request->key;
                $u_table_data = $request->except(['type', 'key']);
                $u_form_data  = DB::table('forms')->where('form_key', $u_key)->first();
                $u_table_name = $u_form_data->table_name;
                $u_field_data = DB::table('form_fields')
                                ->where('form_id', $u_form_data->id)
                                ->get();

                $u_data_to_insert = array();
               // $u_data_to_insert['updated_by'] = $sessionData['user_id'];
                $u_data_to_insert['updated_by'] = !empty($sessionData['user_id']) ? $sessionData['user_id'] :0;
                $u_data_to_insert['updated_at'] = $currentDate->toDateTimeString();

                foreach ($u_field_data as $field) {
                    if (!empty($u_table_data[$field->name])) {
                        $u_data_to_insert[$field->db_mapping] = $u_table_data[$field->name];
                    }
                }
                DB::table($u_table_name)->where('id', $u_entry_id)->update($u_data_to_insert);
            }
            return array('error' => false);
        } catch (\Exception $e) {
            return json_encode(array(['error' => true,'message'=>$e->getMessage(),'line'=>$e->getLine()]));
        }
    }
}
