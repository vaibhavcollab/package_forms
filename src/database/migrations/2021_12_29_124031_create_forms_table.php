<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title', 200);
            $table->integer('display_columns');
            $table->string('form_key', 100);
            $table->string('form_action_route', 100);
            $table->string('table_name', 255);
            $table->tinyInteger('is_save_button');
            $table->tinyInteger('action_edit_button')->nullable();
            $table->tinyInteger('add_button')->nullable();
            $table->tinyInteger('pagination')->nullable();
            $table->tinyInteger('filter_button')->nullable();
            $table->tinyInteger('export_button')->nullable();
            $table->tinyInteger('import_button')->nullable();
            $table->tinyInteger('action_view_button')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->tinyInteger('is_deleted')->nullable()->default('0');
            $table->tinyInteger('is_completed')->nullable()->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forms');
    }
}
