<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogTableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_table', function (Blueprint $table) {
            $table->increments('id');
            $table->string('table_name', 20);
            $table->string('column_name', 20);
            $table->string('previous_value', 255)->nullable();
            $table->string('updated_value', 255)->nullable();
            $table->string('db_user', 50)->default('CURRENT_USER')->nullable();
            $table->string('ref_id', 20)->comment('store row id which is affected on the table')->nullable();
            $table->string('ip_address', 50)->nullable();
            $table->string('localhost_name', 50)->nullable();
            $table->string('device', 20)->default('W')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_table');
    }
}
