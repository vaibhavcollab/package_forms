<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use  Illuminate\Support\Facades\DB;
class FormsAfterUpdateTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("CREATE TRIGGER forms_after_update_trigger AFTER UPDATE ON forms FOR EACH ROW

    BEGIN
  DECLARE GM_STATUS VARCHAR(20);
  IF NEW.title <> OLD.title         THEN
    SET GM_STATUS = insert_general_master_log('forms','title',old.title,new.title,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
  ELSEIF  NEW.display_columns <> OLD.display_columns   THEN
    SET GM_STATUS = insert_general_master_log('forms','display_columns',old.display_columns,new.display_columns,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
  ELSEIF NEW.form_key <> OLD.form_key     THEN
    SET GM_STATUS = insert_general_master_log('forms','form_key',old.form_key,new.form_key,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
  ELSEIF NEW.form_action_route <> OLD.form_action_route       THEN
    SET GM_STATUS = insert_general_master_log('forms','form_action_route',old.form_action_route,new.form_action_route,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
  ELSEIF NEW.table_name <> OLD.table_name THEN
    SET GM_STATUS = insert_general_master_log('forms','table_name',old.table_name,new.table_name,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
  ELSEIF NEW.is_deleted <> OLD.is_deleted       THEN
    SET GM_STATUS = insert_general_master_log('forms','is_deleted',old.is_deleted,new.is_deleted,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
  END IF;
    END

        ");


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP TRIGGER 'forms_after_update_trigger'");
    }
}
